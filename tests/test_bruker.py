import os
from pathlib import Path

import pytest

from fw_gear_file_metadata_importer.files.bruker import (
    fe_merge,
    paravis_extract_files,
    process,
    process_file,
)


@pytest.fixture
def get_meta_acqp():
    acqp_dict = {
        "info": {
            "header": {
                "paravision": {
                    "acq_phase_encoding_mode": "toto",
                    "datatype": "Parameter Values",
                }
            }
        }
    }
    return acqp_dict


@pytest.fixture
def get_meta_method():
    method_dict = {
        "info": {
            "header": {
                "paravision": {
                    "acq_phase_encoding_mode": "titi",
                    "datatype": "Parameter Values",
                }
            }
        }
    }
    return method_dict


def test_process_file(paravision_file):
    fname = "acqp"
    parafile = paravision_file(fname)

    fe, meta, qc = process_file(parafile)

    assert fe["info"]["header"]["paravision"]["datatype"] == "Parameter Values"
    assert meta["file.type"] == "ParaVision"
    assert qc == {}


def test_paravis_extract_files(paravision_file, tmpdir):
    file_name = "Localizer_multi_slice.pv6.zip"
    parazfile = paravision_file(file_name)

    assert os.path.isfile(parazfile)

    acqp, method = paravis_extract_files(parazfile, tmpdir)

    assert acqp == Path(tmpdir) / os.path.splitext(file_name)[0] / "acqp"
    assert method == Path(tmpdir) / os.path.splitext(file_name)[0] / "method"

    assert os.stat(acqp).st_size > 0
    assert os.stat(method).st_size > 0

    file_name = "acqp"
    parazfile = paravision_file(file_name)

    with pytest.raises(SystemExit):
        paravis_extract_files(parazfile, tmpdir)


def test_fe_merge(get_meta_acqp, get_meta_method):
    meta_acqp = get_meta_acqp
    meta_method = get_meta_method

    fe = fe_merge(meta_acqp, meta_method)

    assert isinstance(fe, dict)
    assert len(fe["info"]["header"]["paravision"].keys()) == 2
    assert list(fe["info"]["header"]["paravision"].keys())[0] == "acqp"
    assert list(fe["info"]["header"]["paravision"].keys())[1] == "method"
    assert (
        fe["info"]["header"]["paravision"]["acqp"]["acq_phase_encoding_mode"] == "toto"
    )
    assert (
        fe["info"]["header"]["paravision"]["method"]["acq_phase_encoding_mode"]
        == "titi"
    )

    meta_acqp = {}
    meta_method = get_meta_method

    fe = fe_merge(meta_acqp, meta_method)
    assert len(fe["info"]["header"]["paravision"].keys()) == 2
    assert list(fe["info"]["header"]["paravision"].keys())[0] == "acqp"

    meta_acqp = get_meta_acqp
    meta_method = {}

    fe = fe_merge(meta_acqp, meta_method)

    assert len(fe["info"]["header"]["paravision"].keys()) == 2
    assert list(fe["info"]["header"]["paravision"].keys())[1] == "method"

    meta_acqp = {}
    meta_method = {}
    fe = fe_merge(meta_acqp, meta_method)

    assert len(fe["info"]["header"]["paravision"].keys()) == 2
    fe["info"]["header"]["paravision"]["acqp"] = {}
    fe["info"]["header"]["paravision"]["method"] = {}


def test_process(paravision_file, mocker):
    pf = mocker.patch(
        "fw_gear_file_metadata_importer.files.bruker.paravis_extract_files"
    )
    pf.return_value = ("acqp_path", "method_path")
    pr = mocker.patch("fw_gear_file_metadata_importer.files.bruker.process_file")
    pr.return_value = ({"fe": "bla"}, {"meta", "bla"}, {"qc", "bla"})
    fm = mocker.patch("fw_gear_file_metadata_importer.files.bruker.fe_merge")
    fm.return_value = {"fe": "merge"}

    fe, meta, qc = process("bla")

    assert pr.call_count == 2
    pf.assert_called_once()
    fm.assert_called_once_with({"fe": "bla"}, {"fe": "bla"})


def test_process_file_handles_none_input():
    res = process_file(None)
    assert res == ({"info": {"header": {"paravision": {}}}}, {}, {})
