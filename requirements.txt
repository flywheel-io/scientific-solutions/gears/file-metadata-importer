annotated-types==0.7.0 ; python_version >= "3.10" and python_version < "4.0"
attrs==24.2.0 ; python_version >= "3.10" and python_version < "4.0"
certifi==2024.7.4 ; python_version >= "3.10" and python_version < "4.0"
charset-normalizer==3.3.2 ; python_version >= "3.10" and python_version < "4.0"
colorama==0.4.6 ; python_version >= "3.10" and python_version < "4.0" and platform_system == "Windows"
contourpy==1.3.0 ; python_version >= "3.10" and python_version < "4.0"
cycler==0.12.1 ; python_version >= "3.10" and python_version < "4.0"
decorator==5.1.1 ; python_version >= "3.10" and python_version < "4.0"
dicom-validator==0.3.5 ; python_version >= "3.10" and python_version < "4.0"
dnspython==2.6.1 ; python_version >= "3.10" and python_version < "4.0"
dotty-dict==1.3.1 ; python_version >= "3.10" and python_version < "4.0"
flywheel-gear-toolkit==0.6.18 ; python_version >= "3.10" and python_version < "4.0"
flywheel-gears==0.3.1 ; python_version >= "3.10" and python_version < "4.0"
flywheel-sdk==18.5.0 ; python_version >= "3.10" and python_version < "4.0"
fonttools==4.53.1 ; python_version >= "3.10" and python_version < "4.0"
fw-file==3.4.0 ; python_version >= "3.10" and python_version < "4.0"
fw-meta==4.2.1 ; python_version >= "3.10" and python_version < "4.0"
fw-utils==4.5.0 ; python_version >= "3.10" and python_version < "4.0"
idna==3.8 ; python_version >= "3.10" and python_version < "4.0"
jinja2==3.1.4 ; python_version >= "3.10" and python_version < "4.0"
jsonschema-specifications==2023.12.1 ; python_version >= "3.10" and python_version < "4.0"
jsonschema==4.23.0 ; python_version >= "3.10" and python_version < "4.0"
kiwisolver==1.4.5 ; python_version >= "3.10" and python_version < "4.0"
lazy-loader==0.4 ; python_version >= "3.10" and python_version < "4.0"
markupsafe==2.1.5 ; python_version >= "3.10" and python_version < "4.0"
matplotlib==3.9.2 ; python_version >= "3.10" and python_version < "4.0"
mne==1.7.1 ; python_version >= "3.10" and python_version < "4.0"
natsort==8.4.0 ; python_version >= "3.10" and python_version < "4.0"
nibabel==5.2.1 ; python_version >= "3.10" and python_version < "4.0"
numpy==2.1.0 ; python_version >= "3.10" and python_version < "4.0"
packaging==24.1 ; python_version >= "3.10" and python_version < "4.0"
pillow==10.4.0 ; python_version >= "3.10" and python_version < "4.0"
platformdirs==4.2.2 ; python_version >= "3.10" and python_version < "4.0"
pooch==1.8.2 ; python_version >= "3.10" and python_version < "4.0"
pydantic-core==2.20.1 ; python_version >= "3.10" and python_version < "4.0"
pydantic-settings==2.4.0 ; python_version >= "3.10" and python_version < "4.0"
pydantic==2.8.2 ; python_version >= "3.10" and python_version < "4.0"
pydicom==2.4.4 ; python_version >= "3.10" and python_version < "4.0"
pymongo==4.8.0 ; python_version >= "3.10" and python_version < "4.0"
pyparsing==3.1.4 ; python_version >= "3.10" and python_version < "4.0"
python-dateutil==2.9.0.post0 ; python_version >= "3.10" and python_version < "4.0"
python-dotenv==1.0.1 ; python_version >= "3.10" and python_version < "4.0"
pytz==2021.3 ; python_version >= "3.10" and python_version < "4.0"
pyyaml==6.0.2 ; python_version >= "3.10" and python_version < "4.0"
referencing==0.35.1 ; python_version >= "3.10" and python_version < "4.0"
requests-toolbelt==1.0.0 ; python_version >= "3.10" and python_version < "4.0"
requests==2.32.3 ; python_version >= "3.10" and python_version < "4.0"
rfc3987==1.3.8 ; python_version >= "3.10" and python_version < "4.0"
rpds-py==0.20.0 ; python_version >= "3.10" and python_version < "4.0"
ruamel-yaml==0.16.13 ; python_version >= "3.10" and python_version < "4.0"
scipy==1.14.1 ; python_version >= "3.10" and python_version < "4.0"
six==1.16.0 ; python_version >= "3.10" and python_version < "4.0"
tqdm==4.66.5 ; python_version >= "3.10" and python_version < "4.0"
typed-ast==1.5.5 ; python_version >= "3.10" and python_version < "4.0"
typing-extensions==4.12.2 ; python_version >= "3.10" and python_version < "4.0"
tzdata==2024.1 ; python_version >= "3.10" and python_version < "4.0"
tzlocal==5.2 ; python_version >= "3.10" and python_version < "4.0"
urllib3==2.2.2 ; python_version >= "3.10" and python_version < "4.0"
