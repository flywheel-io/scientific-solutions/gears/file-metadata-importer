#!/usr/bin/env python
"""The run script."""

import logging
import sys
import typing as t

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_file_metadata_importer.main import run
from fw_gear_file_metadata_importer.parser import parse_config
from fw_gear_file_metadata_importer.util import create_metadata

log = logging.getLogger(__name__)


def main(context: GearToolkitContext) -> int:
    """Extract metadata from input file."""
    # parse config
    file_path, file_type, config = parse_config(context)
    file_ = context.get_input("input-file")

    # get parent project
    acquisition = context.client.get_acquisition(file_["hierarchy"]["id"])
    project = context.client.get(acquisition.parents.project)

    # process
    fe, meta, qc = run(
        file_type,
        file_path,
        project=project,
        siemens_csa=config.get("siemens CSA"),
        derived=config.get("derived metadata"),
        p15e=config.get("p15E private tags"),
    )

    # Tag file
    tag = config.get("tag")
    context.metadata.add_file_tags(file_, t.cast(str, tag))

    # create .metadata.json
    create_metadata(context, fe, meta, qc)

    return 0


if __name__ == "__main__":
    with GearToolkitContext(fail_on_validation=False) as context:
        try:
            context.init_logging()
            status = main(context)
        except Exception as exc:
            log.exception(exc)
            status = 1

    sys.exit(status)
