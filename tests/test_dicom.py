import copy
from unittest import mock

import numpy
import pytest
from fw_file.dicom import DICOMCollection

from fw_gear_file_metadata_importer.files.dicom import (
    ARRAY_TAGS,
    DENY_TAGS,
    PRIVATE_TAGS,
    VALID_KEY,
    DICOMFindings,
    add_p15e_tags,
    get_derived_metadata,
    get_dicom_array_header,
    get_dicom_header,
    get_file_info_header,
    get_preamble_dicom_header,
    get_siemens_csa_header,
    inspect_collection,
    inspect_file,
    preprocess_input,
    process,
    update_array_tag,
    validate_geometry,
)


def test_get_Siemens_csa_header(dicom_dataset):
    dcm = dicom_dataset("siemens_with_csa.dcm")
    header = get_siemens_csa_header(dcm)
    assert "image" in header
    assert "series" in header
    assert len(header["series"]) == 47
    assert len(header["image"]) == 30


@pytest.mark.parametrize(
    "zero_byte,decoding,is_valid",
    [
        (False, True, True),
        (False, False, False),
        (True, False, False),
    ],
)
def test_DICOMFindings_is_valid_logic(zero_byte, decoding, is_valid):
    df = DICOMFindings(zero_byte=zero_byte, decoding=decoding, tracking=[])
    assert df.is_valid() == is_valid


def test_DICOMFindings_repr():
    df = DICOMFindings(zero_byte=True, decoding=True, tracking=[])
    assert str(df) == "DICOMFindings:\n\t0-byte: True\n\tdecoding: True"


@pytest.mark.parametrize(
    "key,is_valid",
    [
        ({"00100020": True}, True),
        ({"PatientID": True}, True),
        ({"GEMS_PARM_01, 0043xx01": True}, True),
        ({"0043xx01": True}, False),
        ({"Not_Valid_kw": True}, False),
        ({"PatientID": "True"}, True),
        ({"PatientID": "False"}, True),
        ({"PatientID": "Toto"}, False),
    ],
)
def test_update_array_tag_validate_custom_tags_input(key, is_valid, caplog):
    array_tags = copy.copy(ARRAY_TAGS)
    private_tags = copy.copy(PRIVATE_TAGS)
    deny_tags = copy.copy(DENY_TAGS)
    with mock.patch(
        "fw_gear_file_metadata_importer.files.dicom.ARRAY_TAGS", array_tags
    ), mock.patch(
        "fw_gear_file_metadata_importer.files.dicom.PRIVATE_TAGS", private_tags
    ), mock.patch("fw_gear_file_metadata_importer.files.dicom.DENY_TAGS", deny_tags):
        if is_valid:
            update_array_tag(key)
            assert len(caplog.messages) == 0
        else:
            with pytest.raises(SystemExit) as err:
                update_array_tag(key)
            assert err.type is SystemExit
            assert err.value.code == 1
            assert len(caplog.messages) == 1


@pytest.mark.parametrize(
    "tags,expected_key,is_private",
    [
        ({"00100020": True}, "00100020", False),
        ({"GEMS_PARM_01, 0043xx01": True}, ("GEMS_PARM_01", "0043xx01"), True),
        ({"GEMS_PARM_01,0043xx01": True}, ("GEMS_PARM_01", "0043xx01"), True),
        ({"00100020": "True"}, "00100020", False),
        ({"GEMS_PARM_01, 0043xx01": "true"}, ("GEMS_PARM_01", "0043xx01"), True),
        ({"GEMS_PARM_01,0043xx01": "True"}, ("GEMS_PARM_01", "0043xx01"), True),
    ],
)
def test_update_array_tag_add_to_ARRAY_TAGS_and_PRIVATE_TAGS(
    tags, expected_key, is_private
):
    array_tags = copy.copy(ARRAY_TAGS)
    private_tags = copy.copy(PRIVATE_TAGS)
    with mock.patch(
        "fw_gear_file_metadata_importer.files.dicom.ARRAY_TAGS", array_tags
    ), mock.patch(
        "fw_gear_file_metadata_importer.files.dicom.PRIVATE_TAGS", private_tags
    ):
        k, v = tuple(tags.items())[0]
        assert k not in array_tags
        assert k not in private_tags

        update_array_tag(tags)

        assert expected_key in array_tags
        if is_private:
            assert expected_key in private_tags


@pytest.mark.parametrize(
    "tags,expected_key",
    [
        ({"00100020": False}, "00100020"),
        ({"GEMS_PARM_01, 0043xx01": False}, ("GEMS_PARM_01", "0043xx01")),
        ({"00100020": "False"}, "00100020"),
        ({"GEMS_PARM_01, 0043xx01": "false"}, ("GEMS_PARM_01", "0043xx01")),
    ],
)
def test_update_array_tag_remove_to_ARRAY_TAGS_and_PRIVATE_TAGS(tags, expected_key):
    array_tags = {"00100020", ("GEMS_PARM_01", "0043xx01")}
    private_tags = {("GEMS_PARM_01", "0043xx01")}
    with mock.patch(
        "fw_gear_file_metadata_importer.files.dicom.ARRAY_TAGS", array_tags
    ), mock.patch(
        "fw_gear_file_metadata_importer.files.dicom.PRIVATE_TAGS", private_tags
    ):
        k, v = tuple(tags.items())[0]
        assert expected_key in array_tags

        update_array_tag(tags)

        assert expected_key not in array_tags


def test_update_array_tag_add_to_DENY_TAGS():
    deny_tags = set()
    with mock.patch("fw_gear_file_metadata_importer.files.dicom.DENY_TAGS", deny_tags):
        update_array_tag({"PatientID": False})

        assert "PatientID" in deny_tags


def test_inspect_file_returns_dicom_findings(dicom_dataset):
    dcm = dicom_dataset("siemens_with_csa.dcm", force=True)

    df = inspect_file(dcm)
    assert df.decoding is True
    assert df.zero_byte is False
    assert len(df.tracking) == 0


def test_inspect_collection(dicom_collection):
    coll = dicom_collection("siemens_with_csa.dcm", n=10, force=True)

    res = inspect_collection(coll)

    assert len(res) == 10


def test_get_dicom_header(dicom_dataset):
    dcm = dicom_dataset("siemens_with_csa.dcm", force=True)
    private_tags = {("SIEMENS MR HEADER", "0051xx0A")}

    with mock.patch(
        "fw_gear_file_metadata_importer.files.dicom.PRIVATE_TAGS", private_tags
    ):
        header_with_ob = get_dicom_header(dcm, deny=False)

    assert len(header_with_ob) == 90
    # private tag
    assert "SIEMENS MR HEADER,0051xx0A" in header_with_ob
    # sequence
    assert len(header_with_ob["ReferencedImageSequence"]) == 3
    assert len(header_with_ob["ReferencedImageSequence"][0]) == 2
    # file meta
    assert "MediaStorageSOPClassUID" in header_with_ob
    # deny tag
    assert "PixelData" in dcm
    assert "PixelData" not in header_with_ob


def test_get_preamble_dicom_header_with_deny_tags(dicom_dataset):
    dcm = dicom_dataset("siemens_with_csa.dcm", force=True)
    deny_tags = copy.copy(DENY_TAGS)
    with mock.patch("fw_gear_file_metadata_importer.files.dicom.DENY_TAGS", deny_tags):
        deny_tags.add("ImplementationVersionName")
        header = get_preamble_dicom_header(dcm)

    assert "ImplementationVersionName" not in header


def test_dicom_array_header_multiframe(dicom_collection):
    coll = dicom_collection("HCC_008-CT-3-Phase-MF.dcm", n=1)
    array_header = get_dicom_array_header(coll)
    primary = coll[0]
    n_frames = primary.get("NumberOfFrames")

    ipp = array_header.get("ImagePositionPatient")
    iop = array_header.get("ImageOrientationPatient")
    sl = array_header.get("SliceLocation")

    def assert_is_populated(array):
        assert array is not None
        assert len(array) == n_frames
        for k in range(n_frames):
            assert array[k] is not None

    # First test is to verify that all array_tags are present and populated
    assert_is_populated(ipp)
    assert_is_populated(iop)
    assert_is_populated(sl)


def test_dicom_array_header(dicom_collection):
    coll = dicom_collection("siemens_with_csa.dcm", n=10, force=True)

    array_header = get_dicom_array_header(coll)
    assert len(array_header["EchoTime"]) == 10

    # private tag
    array_tags = {("SIEMENS MR HEADER", "0051xx0A")}
    with mock.patch(
        "fw_gear_file_metadata_importer.files.dicom.ARRAY_TAGS", array_tags
    ):
        array_header = get_dicom_array_header(coll)
        assert len(array_header[("SIEMENS MR HEADER", "0051xx0A")]) == 10


def test_get_file_info_header(dicom_collection):
    coll = dicom_collection("siemens_with_csa.dcm", n=10, force=True)
    dcm = coll[0]

    header = get_file_info_header(dcm, coll, siemens_csa=True)

    assert "dicom" in header
    assert "dicom_array" in header
    assert "csa" in header


def test_get_file_info_header_skips_csa_if_not_siemens(dicom_dataset, caplog):
    dcm = dicom_dataset("ge_sample_with_pdb.dcm")

    caplog.set_level("INFO")
    header = get_file_info_header(dcm, None, siemens_csa=True)
    assert "Manufacturer is not Siemens" in caplog.messages[0]

    assert "csa" not in header


def test_preprocess_input_zip_archive(dicom_file):
    dcm_archive = dicom_file("archive.dicom.zip")

    file_, collection = preprocess_input(dcm_archive)
    assert len(collection) == 5


def test_preprocess_input_dcm(dicom_file):
    dcm = dicom_file("t1_sample.dcm")

    file_, collection = preprocess_input(dcm)
    assert collection is None


def test_preprocess_input_zero_byte_file(dicom_file, mocker):
    validate_mock = mocker.patch("fw_gear_file_metadata_importer.util.validate_file")
    validate_mock.return_value = []
    dcm = dicom_file("zero_byte.dcm.zip")
    with pytest.raises(SystemExit):
        _ = preprocess_input(dcm)


def test_preprocess_input_dcm_log_error_if_validation_errors(dicom_file, caplog):
    dcm = dicom_file("t1_sample.dcm")
    with mock.patch(
        "fw_gear_file_metadata_importer.files.dicom.validate_file",
        return_value=["Some errors"],
    ):
        with pytest.raises(SystemExit) as err:
            preprocess_input(dcm)
        assert err.type is SystemExit
        assert err.value.code == 1
        assert len(caplog.messages) == 1
        assert caplog.messages[0].startswith("Errors found")


@pytest.mark.parametrize(
    "file,startswith",
    [
        ("archive.dicom.zip", "Unable to find a valid Dicom"),
    ],
)
def test_preprocess_input_log_error_if_no_valid_file_in_collection(
    file, startswith, dicom_file, caplog
):
    dcm_archive = dicom_file(file)
    with mock.patch(
        "fw_gear_file_metadata_importer.files.dicom.DICOMFindings.is_valid",
        return_value=False,
    ):
        with pytest.raises(SystemExit) as err:
            preprocess_input(dcm_archive)
        assert err.type is SystemExit
        assert err.value.code == 1
        assert len(caplog.messages) == 1
        assert caplog.messages[0].startswith(startswith)


def test_process(dicom_file, mocker):
    dcm_archive = dicom_file("archive.dicom.zip")
    get_derived_metadata = mocker.patch(
        "fw_gear_file_metadata_importer.files.dicom.get_derived_metadata"
    )
    get_derived_metadata.return_value = ({"bla": "bla"}, ["my-error"])

    fe, meta, qc = process(dcm_archive, derived=True)

    assert fe["modality"] == "MR"
    assert "header" in fe["info"]
    assert qc["filename"] == "siemens_with_csa_1.dcm"
    assert len(qc["trace"]) == 0
    assert len(meta) == 11
    assert qc["derived"] == ["my-error"]
    assert "derived" in fe["info"]["header"]


@pytest.mark.parametrize(
    "ipps,iops,has_error,error_type",
    [
        (None, [], True, "ImagePositionPatient missing"),
        ([], None, True, "ImageOrientationPatient missing"),
        ([], [[0, 1, 0, 1, 0, 0]], True, "not have same length"),
        ([[1.0, 1.0]], [[0, 1, 0, 1, 0, 0]], True, "incorrect length"),
        ([[1.0, 1.0, 2.0]], [[0, 1, 0, 1, 0]], True, "incorrect length"),
        ([[1.0, 1.0, False]], [[0, 1, 0, 1, 0, 0]], True, "incorrect type"),
        ([None], [[0, 1, 0, 1, 0, 0]], True, "ImagePositionPatient missing"),
        ([[1.0, 1.0, 2.0]], [[0, 1, 0, 1, 0, 0]], True, "incorrect type (int)"),
        ([[1.0, 1.0, 2.0]], [[0.0, 1.0, 0.0, 1.0, 0.0, 0.0]], False, ""),
    ],
)
def test_validate_geometry_error(ipps, iops, has_error, error_type):
    collection = mock.MagicMock(spec=DICOMCollection)
    error = validate_geometry(ipps, iops, collection)

    if has_error:
        assert error is not None
        assert error_type in error
    else:
        assert error is None


def test_get_derived_metadata(dicom_file, dicom_dataset):  # noqa: PLR0915
    # A multiframe and single frame version of the same
    # CT dataset with 3 phases
    sf_zip = dicom_file("HCC_008-CT-3-Phase-SF.dicom.zip")
    mf_file = dicom_dataset("HCC_008-CT-3-Phase-MF.dcm")
    mf_coll = DICOMCollection()
    mf_coll.append(mf_file)
    sf_coll = DICOMCollection.from_zip(sf_zip, force=True)
    sf_file = sf_coll[0]

    mf_header = get_file_info_header(mf_file, mf_coll)
    sf_header = get_file_info_header(sf_file, sf_coll)
    # Remove these attributes to test SliceLocation processing
    mf_header["dicom_array"]["SliceLocation"] = []
    sf_header["dicom_array"]["SliceLocation"] = []
    assert mf_header is not None
    assert sf_header is not None

    mf_derived, errors1 = get_derived_metadata(mf_file, mf_coll, mf_header)
    sf_derived, errors2 = get_derived_metadata(sf_file, sf_coll, sf_header)
    assert len(errors1) == 0
    assert len(errors2) == 0
    # All of these values should be present and equal between the sf and mf versions
    attr_list = [
        "MaxSliceLocation",
        "MinSliceLocation",
        "ScanCoverage",
        "SliceLocation",
        "SliceAxis",
        "PhaseCount",
        "PhaseIndexes",
    ]
    for attr in attr_list:
        v1 = mf_derived.get(attr)
        v2 = sf_derived.get(attr)
        assert v1 is not None
        assert v2 is not None
        if isinstance(v1, list):
            assert numpy.array_equal(v1, v2)
        else:
            assert numpy.isclose(v1, v2)

    # These collections should have a phase count of 3
    assert sf_derived.get("PhaseCount") == 3

    # These full multiphasic versions should NOT have slice spacing or an affine transform
    slice_attrs = ["SpacingBetweenSlices", "affine", "fov"]
    for attr in slice_attrs:
        assert attr not in mf_derived
        assert attr not in sf_derived

    # Now take first phase of single frame collection consisting of 80 slices
    sf_coll2 = DICOMCollection()
    for index in range(80):
        sf_coll2.append(sf_coll[index])
    sf_header2 = get_file_info_header(sf_file, sf_coll2)
    sf_derived2, errors3 = get_derived_metadata(sf_file, sf_coll2, sf_header2)
    # Now all these attributes SHOULD be present in the single phase version
    for attr in slice_attrs:
        assert attr in sf_derived2
    # FOV should be consistent with scan coverage
    fov = sf_derived2.get("fov")
    scan_coverage = sf_derived2.get("ScanCoverage")
    assert numpy.isclose(fov[2], scan_coverage)
    assert numpy.isclose(sf_derived2.get("SpacingBetweenSlices"), 2.5)
    assert sf_derived2.get("PhaseCount") == 1
    assert len(errors3) == 0

    # Now verify error if data is missing 3D info
    del sf_file.dataset["ImagePositionPatient"]
    sf_header3 = get_file_info_header(sf_file, sf_coll2)
    sf_derived3, errors4 = get_derived_metadata(sf_file, sf_coll2, sf_header3)
    assert len(errors4) > 0

    # Now verify processing on single slice data set
    sf_coll3 = DICOMCollection()
    sf_file = sf_coll[2]
    sf_coll3.append(sf_file)
    sf_header4 = get_file_info_header(sf_file, sf_coll3)
    assert sf_header4 is not None
    sf_derived4, errors5 = get_derived_metadata(sf_file, sf_coll3, sf_header4)
    assert len(errors5) == 0
    for attr in slice_attrs:
        assert attr not in sf_derived4

    # Now verify processing on data set with degenerate slices
    sf_coll3.append(sf_file)
    sf_header5 = get_file_info_header(sf_file, sf_coll3)
    assert sf_header5 is not None
    sf_derived5, errors6 = get_derived_metadata(sf_file, sf_coll3, sf_header5)
    assert len(errors6) == 0
    for attr in slice_attrs:
        assert attr not in sf_derived5


def test_add_p15e_tags(mocker):
    mocked_tag_return = mocker.patch(
        "fw_gear_file_metadata_importer.files.dicom.load_p15e_tags_from_file"
    )
    mocked_tag_return.return_value = {
        ("creator_1", "1234xx01"),
        ("creator_2", "1234xx02"),
    }
    assert PRIVATE_TAGS == set()
    add_p15e_tags()
    assert len(PRIVATE_TAGS) == 2
    assert ("creator_1", "1234xx01") in PRIVATE_TAGS
    assert ("creator_2", "1234xx02") in PRIVATE_TAGS


@pytest.mark.parametrize(
    "key,expected",
    [
        ("00100020", True),
        ("PatientID", True),
        ("GEMS_PARM_01, 0043xx01", True),
        ("GEMS_PARM_01, AB43xxFF", True),
        ("0043xx01", False),
        ("Not_Valid_kw", False),
    ],
)
def test_VALID_KEY_regex(key, expected):
    assert (VALID_KEY.match(key) is not None) == expected
