import math

import numpy as np
import pytest
from numpy import ndarray

from fw_gear_file_metadata_importer import geometry

X_AXIS = [1.0, 0.0, 0.0]
Y_AXIS = [0.0, 1.0, 0.0]
Z_AXIS = [0.0, 0.0, 1.0]

AXIAL = Y_AXIS + X_AXIS
SAG = Z_AXIS + Y_AXIS
COR = Z_AXIS + X_AXIS


def get_random_unit_normals(n=100):
    """
    Compute a list of random unit normals

    Args:
        n (int): Number of vectors to compute

    Returns:
        list: List of vectors that are random unit normals
    """
    unit_normals = []
    # Used to generate random 3D unit normal vectors
    v = np.random.rand(n * 3)
    v_list = v.reshape(n, 3)
    for v in v_list:
        v_hat = v / np.linalg.norm(v)
        unit_normals.append(v_hat)
    return unit_normals


def test_compute_normal():
    vx = [1.0, 0, 0]
    vy = [0, 1.0, 0]
    vz = [0, 0, 1.0]
    t1 = geometry.compute_normal(vx + vy)
    t2 = geometry.compute_normal(vz + vx)
    t3 = geometry.compute_normal(vy + vz)
    t_degenerate = geometry.compute_normal(vx + vx)
    assert np.allclose(t1, vz)
    assert np.allclose(t2, vy)
    assert np.allclose(t3, vx)
    assert np.allclose(t_degenerate, [0, 0, 0])


def test_compute_axis_from_pair():
    # Verify that method is robust for null inputs
    assert geometry.compute_axis_from_pair(None, (0, 0, 0)) is None
    assert geometry.compute_axis_from_pair((0, 0, 0), None) is None

    axes = get_random_unit_normals()
    for axis in axes:
        t_axis = geometry.compute_axis_from_pair(2 * axis, 4 * axis)
        assert np.isclose(np.linalg.norm(t_axis), 1)
        assert np.isclose(np.dot(t_axis, axis), 1)


def test_compute_axis_from_origins():
    # Verify null return when there is < 2 data points
    assert geometry.compute_axis_from_origins([]) is None
    assert geometry.compute_axis_from_origins([0]) is None

    axes = get_random_unit_normals()
    for axis in axes:
        # Verify null return when list is degenerate
        assert geometry.compute_axis_from_origins([axis, axis, axis]) is None

        origin_list = [axis, axis, 2 * axis]
        # Check the value comes back even if the first two points are degenerate
        t_axis = geometry.compute_axis_from_origins(origin_list)
        assert t_axis is not None
        assert np.allclose(axis, t_axis)


def test_compute_slice_locations_from_origins():
    axes = get_random_unit_normals()
    for axis in axes:
        origin_list = [axis, 3 * axis, 5 * axis]
        # Check with even slice spacing
        locations = geometry.compute_slice_locations_from_origins(origin_list)
        assert np.allclose(locations, [1, 3, 5])
        # Check with non uniform slice spacing
        origin_list2 = [axis, 3 * axis, 5 * axis, 6 * axis]
        locations2 = geometry.compute_slice_locations_from_origins(origin_list2)
        assert np.allclose(locations2, [1, 3, 5, 6])
        # Test null condition when slices are degenerate
        origin_list3 = [axis, axis, axis]
        locations3 = geometry.compute_slice_locations_from_origins(origin_list3)
        assert locations3 is None


def test_compute_slice_spacing_from_locations():
    # Verify null spacing if there is only 1 location
    assert geometry.compute_slice_spacing_from_locations([0]) is None

    axes = get_random_unit_normals()
    for axis in axes:
        origin_list = [axis, 3 * axis, 5 * axis]
        # Verify regular spacing
        locations = geometry.compute_slice_locations_from_origins(origin_list)
        spacing = geometry.compute_slice_spacing_from_locations(locations)
        assert np.isclose(spacing, 2)
        origin_list2 = [axis, 3 * axis, 5 * axis, 6 * axis]
        # Verify null spacing for non uniform interval
        locations2 = geometry.compute_slice_locations_from_origins(origin_list2)
        spacing2 = geometry.compute_slice_spacing_from_locations(locations2)
        assert spacing2 is None


@pytest.mark.parametrize(
    "v, expected_uniform",
    [
        [[2, 2, 2, 2], True],
        [[2, 2, 2, 2.001], True],
        [[2, 2, 2, 2.01], False],
        [[[0, 1, 0, 1, 0, 0], [0, 1, 0, 1, 0, 0]], True],
        [[[0, 1, 0, 1, 0, 0], [0, 1, 0, 0, 0, 1]], False],
    ],
)
def test_is_uniform(v, expected_uniform):
    v = np.asarray(v)
    if expected_uniform:
        assert geometry.is_uniform(v, decimals=2)
    else:
        assert not geometry.is_uniform(v, decimals=2)
    if len(v) == 4:
        v = v.reshape(2, 2)
        if expected_uniform:
            assert geometry.is_uniform(v, decimals=2)
        else:
            assert not geometry.is_uniform(v, decimals=2)


def _compute_axes_origins(args: list[tuple[int, ndarray]]) -> tuple[ndarray, ndarray]:
    """
    Compute axes and origins from a sequence of length, axis

    Args:
        args (list[tuple[int,ndarray]]) : A list of tuples where each
        tuple consists of 2 elements:
          - length (int) : Number of uniformly spaced origins to compute
          - axis (ndarray) : Unit axis connecting the origins

    Returns:
        tuple[ndarray,ndarray]: A tuple of 2 elements:
          - axes (ndarray) : List repeating the input axis
          - origins (ndarray) : Corresponding list of uniformly spacing origins
    """
    origins = []
    axes = []
    # Iterate through the args to get the length, axis pair
    for length, axis in args:
        for k in range(length):
            axes.append(axis)
            origin = []
            for dim in range(3):
                origin.append(axis[dim] * float(k))
            origins.append(origin)
    return axes, origins


def _compute_orientations_origins_along_locations(
    orientation: ndarray, locations: ndarray
) -> tuple[ndarray, ndarray]:
    """
    Compute axes and origins from an orientation and locations

    Args:
        orientation (ndarray): A vector corresponding to ImageOrientationPatient
        locations (ndarray): Vector of scalar locations along an axis

    Returns:
        tuple[ndarray,ndarray]: A tuple of 2 elements:
          - axes (ndarray) : List corresponding to the axis for the orientation
          - origins (ndarray) : Corresponding list of uniformly spacing origins
    """
    n = len(locations)
    origins = []
    axes = []
    axis = geometry.compute_normal(orientation)
    for k in range(n):
        origin = axis * float(locations[k])
        origins.append(origin)
        axes.append(orientation)
    return axes, origins


def compute_radial_geometry(n: int = 12, fov=200.0):
    """
    Compute a set of orientations and origins for radial geometry

    Args:
        n (int): Number of times to sample a full rotation
        fov (float) : Length ("field of view") in mm along each radial plane

    Returns:
        list : Orientations corresponding to the radial geometry
    """

    def rotation_matrix(theta) -> ndarray:
        cos_theta = math.cos(theta)
        sin_theta = math.sin(theta)
        matrix = []
        matrix.append([cos_theta, -sin_theta, 0])
        matrix.append([sin_theta, cos_theta, 0])
        matrix.append([0, 0, 1.0])
        return np.asarray(matrix)

    orient_list = []
    angle = 360.0 / float(n)
    rot_radians = np.radians(angle)
    for k in range(n):
        rot_angle = rot_radians * float(k)
        rot_matrix = rotation_matrix(rot_angle)
        v1 = np.asarray(COR[0:3], dtype=float)
        v2 = np.asarray(COR[3:6], dtype=float)
        v1 = np.matmul(rot_matrix, v1)
        v2 = np.matmul(rot_matrix, v2)
        orientation = v1.tolist() + v2.tolist()
        orient_list.append(orientation)
    return orient_list


def test_compute_phases_by_orientation(split_by_geometry_func=None):
    if split_by_geometry_func is None:
        split_by_geometry_func = geometry.split_by_geometry

    arrays = [[], [[0, 0, 1, 1, 0, 0]]]
    for orient_list in arrays:
        phase_count, indexes = geometry.split_by_orientation(orient_list)
        assert phase_count == 1
        assert len(indexes) == len(orient_list)

    # Verify no split for uniform orientation along all 3 ortho orientations
    ortho_orientation = [AXIAL, SAG, COR]
    for idx in range(3):
        orientation = np.asarray(ortho_orientation[idx])
        orient_list1 = []
        n_pos = 10
        for k in range(n_pos):
            orient_list1.append(orientation)
        phase_count, indexes = geometry.split_by_orientation(orient_list1, max_split=3)
        assert phase_count == 1
        assert len(indexes) == len(orient_list1)

    # Verify no split when orientations consistently change
    orient_list2 = compute_radial_geometry()
    phase_count2, indexes2 = geometry.split_by_orientation(orient_list2, max_split=3)
    assert phase_count2 == 1
    assert len(indexes2) == len(orient_list2)

    # Construct set of orthogonal orientations
    args = [(5, AXIAL), (4, COR), (3, SAG)]
    orient_list2, origins2 = _compute_axes_origins(args)

    # Verify proper split by orientation for ortho orientations
    phase_count2, indexes2 = geometry.split_by_orientation(orient_list2, max_split=3)
    assert phase_count2 == 3
    assert (indexes2[0], indexes2[5], indexes2[9]) == (0, 1, 2)
    assert (indexes2[4], indexes2[8], indexes2[11]) == (0, 1, 2)

    # Verify split by geometry is equivalent to split by orientation for ortho case
    phase_count3, indexes3 = split_by_geometry_func(orient_list2, origins2, max_split=3)
    assert phase_count3 == 3
    assert (indexes3[0], indexes3[5], indexes3[9]) == (0, 1, 2)
    assert (indexes3[4], indexes3[8], indexes3[11]) == (0, 1, 2)


# This tests a very specific condition within split_by_location where the phases
# are encoded by a set of degenerate slice locations
def test_compute_phases_with_deg_slice_locations(split_by_geometry_func=None):
    locations = []
    phase1 = range(0, 10, 2)
    lp1 = len(phase1)
    n_phases = 3
    for k in phase1:
        for m in range(n_phases):
            locations.append(k)
    phase_count, indexes = geometry.split_by_location(locations)
    assert phase_count == n_phases
    assert len(indexes) == len(locations)
    # Test that indexes are assigned phases of 0,1,2, 0,1,2, 0,1,2,...
    for k in range(lp1):
        assert indexes[n_phases * k] == 0
        assert indexes[n_phases * k + 1] == 1
        assert indexes[n_phases * k + 2] == 2


def test_compute_phases_by_location(split_by_geometry_func=None):
    if split_by_geometry_func is None:
        split_by_geometry_func = geometry.split_by_geometry

    # Test empty and 1 length arrays
    arrays = [[], [0]]
    for loc in arrays:
        phase_count, indexes = geometry.split_by_location(loc)
        assert phase_count == 1
        assert len(indexes) == len(loc)

    # Test the phase condition of overlapping slice locations
    phase1 = range(0, 100, 2)
    phase2 = range(20, 124, 2)
    locations = []
    for k in phase1:
        locations.append(k)
    for k in phase2:
        locations.append(k)
    lp1 = len(phase1)
    lp2 = len(phase2)
    phase_count, indexes = geometry.split_by_location(locations)
    assert phase_count == 2
    assert len(locations) == len(indexes)

    # Verify that phase indexes corresponding to phase1 are all equal to zero
    m1 = np.full(shape=lp1, fill_value=0)
    assert np.array_equiv(m1, indexes[0:lp1])

    # Verify that phase indexes corresponding to phase2 are all equal to 1
    idx2 = indexes[lp1 : lp1 + lp2]
    m2 = np.full(shape=lp2, fill_value=1)
    assert np.array_equiv(m2, idx2)

    # Verify that split by geometry is controlled by location for this use case
    orientations, origins = _compute_orientations_origins_along_locations(
        [1, 0, 0, 0, 1, 0], locations
    )
    phase_count2, indexes2 = split_by_geometry_func(orientations, origins)
    assert phase_count2 == 2

    # Verify max splits are enforced
    locations5 = []
    for k in phase1:
        for m in range(4):
            locations5.append(k)
    phase_count5, indexes5 = geometry.split_by_location(locations5, max_split=3)
    assert phase_count5 == 1
