import numpy

from fw_gear_file_metadata_importer import geometry
from fw_gear_file_metadata_importer.transform import (
    Matrix3DTransform,
    Transform,
    apply_transform_to_point,
    apply_transform_to_points,
)


def test_base_transform():
    # Just generate base class to verify stubs for code coverage goal
    transform = Transform()
    point = (1, 1, 1)
    p2 = transform.transform_point(point)
    assert point == p2
    p3 = transform.inverse_transform_point(point)
    assert point == p3
    p4 = transform.transform_points(point)
    assert point == p4
    p5 = transform.inverse_transform_points(point)
    assert point == p5
    p6 = transform.transform_vector(point)
    assert point == p6
    p7 = transform.inverse_transform_vector(point)
    assert point == p7


def _get_affine(
    origin=(0, 0, 0),
    row_axis=[1, 0, 0],
    col_axis=[0, 1, 0],
    voxels=[0.72, 0.72, 2.5],
    x_offset=0,
    y_offset=0,
):
    row_axis = numpy.asarray(row_axis, dtype=float)
    col_axis = numpy.asarray(col_axis, dtype=float)
    voxels = numpy.asarray(voxels, dtype=float)
    p1 = numpy.asarray(origin, dtype=float)
    p2 = p1.copy()
    p2[0] += x_offset
    p2[1] += y_offset
    p2[2] += voxels[2]
    slice_axis = geometry.compute_axis_from_pair(p1, p2)
    axes = [row_axis, col_axis, slice_axis]
    return geometry.compute_affine(axes=axes, origin=p1, spacing=voxels)


def test_apply_transform_to_point():
    origin = numpy.array([100, 200, -50], dtype=float)
    y_offset = 0.01
    voxels = [0.72, 0.72, 2.5]
    affine = _get_affine(origin, y_offset=y_offset, voxels=voxels)
    p1 = apply_transform_to_point(affine, (0, 0, 0))
    assert len(p1) == len(origin)
    assert numpy.allclose(p1, origin)
    p2 = apply_transform_to_point(affine, (0, 0, 1))
    assert numpy.allclose(p2[1], origin[1] + y_offset)


def test_apply_transform_to_points():
    affine = _get_affine()
    result_list1 = []
    point_list = []
    for dim in range(0, 3):
        point = [0.0, 0.0, 0.0]
        for index in range(1, 4):
            point[dim] = index
            point_list.append(numpy.asarray(point, dtype=float))
            result_list1.append(apply_transform_to_point(affine, point))
    # Verify that the results of apply_transform_to_points are consistent
    # with those of apply_transform_to_points
    result_list1 = numpy.asarray(result_list1)
    result_list2 = apply_transform_to_points(affine, point_list)
    assert numpy.array_equal(result_list1, result_list2)

    # Test 2D case
    result_list1 = []
    point_list = []
    for dim in range(0, 2):
        point = [0.0, 0.0]
        for index in range(1, 4):
            point[dim] = index
            point_list.append(numpy.asarray(point, dtype=float))
            result_list1.append(apply_transform_to_point(affine, point))
    # Verify that the results of apply_transform_to_points are consistent
    # with those of apply_transform_to_points
    result_list1 = numpy.asarray(result_list1)
    result_list2 = apply_transform_to_points(affine, point_list)
    assert numpy.array_equal(result_list1, result_list2)


def _get_random_grid_points(n_points=100, extent=100.0):
    rv = numpy.random.rand(n_points * 3)
    rv *= extent
    return rv.reshape(n_points, 3)


def test_m3d_transform_point():
    origin = numpy.asarray([100, 200, -50], dtype=float)
    transform = Matrix3DTransform(_get_affine(origin, y_offset=0.02))
    assert transform is not None
    p1 = transform.transform_point([0, 0, 0])
    assert len(p1) == len(origin)
    assert numpy.allclose(p1, origin)
    row_step = transform.transform_vector([0, 1, 0])
    col_step = transform.transform_vector([1, 0, 0])
    slice_step = transform.transform_vector([0, 0, 1])

    # Verify that transform point is consistent with transform_vector
    # and equivalent number of steps along each dimension
    rv_coords = _get_random_grid_points()
    for rv in rv_coords:
        p1 = transform.transform_point(rv)
        p2 = origin.copy()
        p2 += rv[0] * col_step
        p2 += rv[1] * row_step
        p2 += rv[2] * slice_step
        assert numpy.allclose(p1, p2)


def test_m3d_inverse_transform_point():
    origin = numpy.asarray([100, 200, -50], dtype=float)
    transform = Matrix3DTransform(_get_affine(origin, y_offset=0.02))

    # Verify that inverse performs opposite transform to forward
    rv_coords = _get_random_grid_points()
    for rv in rv_coords:
        p1 = transform.transform_point(rv)
        p2 = transform.inverse_transform_point(p1)
        assert numpy.allclose(p2, rv)


def test_m3d_transform_vector():
    # Already verified by test_m3d_transform_point
    pass


def test_m3d_inverse_transform_vector():
    origin = numpy.asarray([100, 200, -50], dtype=float)
    transform = Matrix3DTransform(_get_affine(origin, y_offset=0.02))
    row_unit = numpy.asarray([0, 1, 0], dtype=float)
    col_unit = numpy.asarray([1, 0, 0], dtype=float)
    slice_unit = numpy.asarray([0, 0, 1], dtype=float)
    row_step = transform.transform_vector(row_unit)
    col_step = transform.transform_vector(col_unit)
    slice_step = transform.transform_vector(slice_unit)
    v1 = transform.inverse_transform_vector(row_step)
    v2 = transform.inverse_transform_vector(col_step)
    v3 = transform.inverse_transform_vector(slice_step)
    assert numpy.allclose(v1, row_unit)
    assert numpy.allclose(v2, col_unit)
    assert numpy.allclose(v3, slice_unit)


def test_m3d_transform_points():
    affine = _get_affine()
    transform = Matrix3DTransform(affine)
    result_list1 = []
    point_list = []
    for dim in range(0, 3):
        point = [0.0, 0.0, 0.0]
        for index in range(1, 4):
            point[dim] = index
            point_list.append(numpy.asarray(point, dtype=float))
            result_list1.append(transform.transform_point(point))
    # Verify that the results of transform_points are consistent
    # with those of transform_point
    result_list1 = numpy.asarray(result_list1)
    result_list2 = transform.transform_points(point_list)
    assert numpy.array_equal(result_list1, result_list2)


def test_m3d_inverse_transform_points():
    affine = _get_affine()
    transform = Matrix3DTransform(affine)
    point_list = []
    for dim in range(0, 3):
        point = [0.0, 0.0, 0.0]
        for index in range(1, 4):
            point[dim] = index
            point_list.append(numpy.asarray(point, dtype=float))
    # Verify that the results of transform_points are consistent
    # with those of transform_point
    result_list1 = transform.transform_points(point_list)
    result_list2 = transform.inverse_transform_points(result_list1)
    result_list2 = numpy.round(result_list2, decimals=5)
    point_list = numpy.round(point_list, decimals=5)
    assert numpy.array_equal(result_list2, point_list)
