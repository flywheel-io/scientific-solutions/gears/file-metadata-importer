# Release Notes

## 1.5.5

__Fixes__:

* Fix regular expression for extracting Private tag with non "numeric" hex character.

## 1.5.4

__Maintenance__:

* Push to dockerhub and not only gitlab registry

## 1.5.2

__Enhancements__:

* Updates behavior around `PerFrameFunctionalGroupsSequence` so that only the first
frame is retained

## 1.5.1

__Maintenance__:

* Update QA-CI
* Update dependencies

## 1.5.0

__Maintenance__:

* Add mkdocs documentation to repo
* Update qa-ci

__Enhancements__:

* Add support for zipped PAR/REC files
* Add `output_configuration` to manifest to ensure file version matching
* Add config option to import p15e private tags

## 1.4.1

__Enhancements__:

* Add support for EEG files (BrainVision, EDF, BDF, EEGLAB)

__Maintenance__:

* Update fw-file to 3.3.1
* Update flywheeel-sdk to ^17
* Update CI/CD pipeline

## 1.4.0

__Fixes__:

* Only writes SliceLocation to derived if it doesn't already exists in dicom_array.

 __Enhancements__:

* Updates to fw-file >3.0.1

## 1.3.0

__Fixes__:

* DICOM array is now populated for multiframe case

 __Enhancements__:

* Updates to fw-file 3.0.0
* Add "deny VR" feature
* New derived attributes:
  * `SliceAxis` : 3D unit vector connecting slice origins
  * `SpacingBetweenSlices` [type 3 tag at (0018,0088), empty if slice spacing is not
    uniform]
  * `fov` : "Field of view" which is 3D span in patient coords along x,y,z (empty if
    slice spacing is not uniform)
  * `affine` : 4x4 affine transform to convert grid to patient coords (empty if slice
    spacing is not uniform)

## 1.2.5

__Enhancements__:

* Improve logging
  * Of metadata
  * Of gear run
* Improve handling of metadata

## 1.2.4

__Fixes__:

* Don't attempt to calculate derived metadata for a single dicom input.

## 1.2.3

__Fixes__:

* Don't mark gear as failed on metadata validation failure.

## 1.2.2

__Fixes__:

* Fix remove empty behavior when value is a dicom Tag.

## 1.2.1

__Fixes__:

* Fix modality field in metadata

## 1.2.0

__Enhancements__:

* Add support for PFILE.
* Add support for Philips PAR/REC files.

## 1.1.0

__Enhancements__:

* Add `derived` key in header to report on derived metadata. Currently only ScanCoverage
  for CT modality.
* Add support for Bruker Paravision files.

## 1.0.3

__Fixes__:

* Fix session age inference when timezone is set for AcquisitionDateTime (fw-file).

__Maintenance__:

* Update dependencies (gear-toolkit)

## 1.0.2

__Fixes__:

* Convert the NaN value parsed from NIfTI header to None.  

## 1.0.1

__Fixes__:

* Exception handling when Modality is undefined.
* Upgrade gear-toolkit with fix to handle bytes extracted from file headers.

__Enhancements__:

* Add support for NIfTI.
* Add support for PTD.
* Add support for config option for tag.

__Maintenance__:

* Don't report on tracking events that are inferring VR from implicit VR dicoms
  (breaking change).
* Restructure repo to allow for better organization of more file types.
* Make determination of zip archive more robust.
