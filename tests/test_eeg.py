import pytest

from fw_gear_file_metadata_importer.files.eeg import (
    process_bdf,
    process_brainvision,
    process_edf,
    process_eeglab,
)
from tests.conftest import ASSETS_ROOT


def test_process_brainvision_zipped():
    fname = ASSETS_ROOT / "EEG/brainvision.zip"
    fe, meta, qc = process_brainvision(fname)

    assert fe["info"]["header"]["brainvision"]["chs"]
    assert meta["file.type"] == "eeg"
    assert qc == {}


def test_process_brainvision_header_raises():
    fname = ASSETS_ROOT / "EEG/Analyzer_nV_Export.vhdr"
    with pytest.raises(RuntimeError):
        process_brainvision(fname)


def test_process_edf():
    fname = ASSETS_ROOT / "EEG/chtypes_edf.edf"
    fe, meta, qc = process_edf(fname)

    assert fe["info"]["header"]["edf"]["chs"]
    assert meta["file.type"] == "eeg"
    assert qc == {}


def test_process_bdf():
    fname = ASSETS_ROOT / "EEG/test_bdf_stim_channel.bdf"
    fe, meta, qc = process_bdf(fname)

    assert fe["info"]["header"]["bdf"]["chs"]
    assert meta["file.type"] == "eeg"
    assert qc == {}


def test_process_eeglab_single_file():
    fname = ASSETS_ROOT / "EEG/test_raw_onefile.set"
    fe, meta, qc = process_eeglab(fname)

    assert fe["info"]["header"]["eeglab"]["chs"]
    assert meta["file.type"] == "eeg"
    assert qc == {}


def test_process_eeglab_zipped():
    fname = ASSETS_ROOT / "EEG/eeglab.zip"
    fe, meta, qc = process_eeglab(fname)

    assert fe["info"]["header"]["eeglab"]["chs"]
    assert meta["file.type"] == "eeg"
    assert qc == {}
