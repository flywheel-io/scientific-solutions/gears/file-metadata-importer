from fw_gear_file_metadata_importer.files.nifti import decode, process


def test_process(asset_file):
    fname = "t2_file.nii.gz"
    nifti = asset_file(fname)

    fe, meta, qc = process(nifti)

    assert fe["info"]["header"]["nifti"]["magic"] == "n+1"
    assert meta["file.type"] == "nifti"
    assert qc == {}


def test_decode():
    val = b"a-val"
    assert decode(val) == "a-val"


def test_decode_report_hex_value_if_unicodedecodeerror(mocker):
    val = "bla".encode("utf-16")
    assert decode(val) == val.hex()
