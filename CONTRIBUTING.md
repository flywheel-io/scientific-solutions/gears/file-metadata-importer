# Contributing

## Getting started

1. Follow instructions to [install poetry](https://python-poetry.org/docs/#installation).
2. Follow instructions to [install pre-commit](https://pre-commit.com/#install)

After cloning the repo:

1. `poetry install`: Install project and all dependencies (see **Dependency
management** below)
2. `pre-commit install`: Install pre-commit hooks (see **Linting and Testing** below)

## Dependency management

This gear uses [`poetry`](https://python-poetry.org/) to manage dependencies,
develop, build and publish.

### Dependencies

Dependencies are listed in the `pyproject.toml` file.

#### Managing dependencies

- Adding: Use `poetry add [--dev] <dep>`
- Removing: Use `poetry remove [--dev] <dep>`
- Updating: Use `poetry update <dep>` or `poetry update` to update all deps.
  - Can also not udpate development dependencies with `--no-dev`
  - Update dry run: `--dry-run`

#### Using a different version of python

Poetry manages virtual environments and can create a virtual environment with different
versions of python, however that version must be installed on the machine.

You can configure the python version by using `poetry env use <path/to/executable>`

#### Helpful poetry config options

See full options [Here](https://python-poetry.org/docs/configuration/#available-settings).

List current config: `poetry config --list`

- `poetry config virtualenvs.in-project <true|false|None>`: create virtual environment
inside project directory
- `poetry config virtualenvs.path <path>`: Path to virtual environment directory.

## Linting and Testing

Local linting and testing scripts are managed through
[`pre-commit`](https://pre-commit.com/).

### pre-commit usage

- Run hooks manually:
  - Run on all files: `pre-commit run -a`
  - Run on certain files: `pre-commit run --files test/*`
- Update hooks: `pre-commit autoupdate`
- Disable all hooks: `pre-commit uninstall`
- Enable all hooks: `pre-commit install`
- Skip a hook on commit: `SKIP=<hook-name> git commit`
- Skip all hooks on commit: `git commit --no-verify`

## Adding a new file type

Adding a new file type is fairly straight forward.

### Step 1: Add <file_type>.process() function

Within the main `fw_gear_file_metadata_import` package, add a new python file
corresponding to your file type, and expose a method called `process` which takes in a
file path and any keyword arguments.

This function `process` should return:

- A dictionary of file attributes to update (i.e. info, modality, type)
- File metadata in the format of `fw-meta.MetaData`, this can be returned if your file
type is supported by `fw-file` as `File.meta`.
- A dictionary of any QC data to store on the file info. Should at minimum contain the
key `filename` with the name of the file as the value.

### Step 2: Add your file type to parsing workflow

In `fw_gear_file_metadata_import.main.run`, add either your file type to the branching
if-elif-else statements. If your file type doesn't have a specific type, or you need to
 get more specific, you can use things such as file suffix to further match your file.
