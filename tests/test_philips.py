from fw_gear_file_metadata_importer.files.philips import process


def test_process(asset_file):
    fname = "rf_wipsurvey_v42.par"
    parrec = asset_file(fname)

    fe, meta, qc = process(parrec)

    assert fe["info"]["header"]["par"]["patient_name"] == "rf"
    assert meta["file.type"] == "parrec"
    assert qc == {}


def test_process_zip(asset_file):
    fe, meta, qc = process("tests/assets/PARREC/parrec_single.zip")

    assert fe["info"]["header"]["par"]["patient_name"] == "phantom"
    assert meta["file.type"] == "parrec"
    assert qc == {}
