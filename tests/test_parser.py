from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_file_metadata_importer.parser import parse_config
from tests.conftest import ASSETS_ROOT


def test_parse_config():
    context = GearToolkitContext(config_path=ASSETS_ROOT / "config.json", input_args=[])
    file_path, file_type, config = parse_config(context)
    assert file_path == "/some/path/tmp.dicom.zip"
    assert file_type == "dicom"
    assert config == {"debug": True}
