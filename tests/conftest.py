import functools
import shutil
import tempfile
from pathlib import Path

import pytest
from fw_file.dicom import DICOM, DICOMCollection

ASSETS_ROOT = Path(__file__).parent / "assets"
DICOM_ROOT = ASSETS_ROOT / "DICOM"
PARAVISION_ROOT = ASSETS_ROOT / "PARAVISION"
PARREC_ROOT = ASSETS_ROOT / "PARREC"


def get_asset_file(root_dir, filename):
    """Returns path to file in assets."""
    if not isinstance(filename, Path):
        filename = Path(filename)
    path = Path(tempfile.mkdtemp())
    shutil.copy(root_dir / filename, path / filename)
    return str(path / filename)


@pytest.fixture
def dicom_file():
    get_dicom_file = functools.partial(get_asset_file, DICOM_ROOT)
    return get_dicom_file


@pytest.fixture
def paravision_file():
    get_dicom_file = functools.partial(get_asset_file, PARAVISION_ROOT)
    return get_dicom_file


@pytest.fixture
def parrec_file():
    get_parrec_file = functools.partial(get_asset_file, PARREC_ROOT)
    return get_parrec_file


@pytest.fixture
def asset_file():
    get_asset = functools.partial(get_asset_file, ASSETS_ROOT)
    return get_asset


@pytest.fixture
def dicom_dataset(dicom_file):
    def get_dicom_dataset(filename, **kwargs):
        path = dicom_file(filename)
        return DICOM(path, **kwargs)

    return get_dicom_dataset


@pytest.fixture
def dicom_collection(dicom_dataset):
    def get_dicom_collection(filename, n=10, **kwargs):
        dcm = dicom_dataset(filename, **kwargs)
        coll = DICOMCollection()
        for i in range(n):
            coll.append(dcm)
        return coll

    return get_dicom_collection
