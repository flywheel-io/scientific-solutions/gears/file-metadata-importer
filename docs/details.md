# Gear Details

This section provides detailed information about the gear, including its inputs,
outputs, and configuration options.

## License

[MIT
License](https://gitlab.com/flywheel-io/scientific-solutions/gears/file-metadata-importer/-/blob/main/LICENSE)

## Classification

* **Species:** All
* **Organ:** Other
* **Therapeutic Area:** Any
* **Modality:** Any
* **File Type:** DICOM, NIfTI, ParaVision, PAR/REC, EEG (BrainVision, EDF, BDF, EEGLAB)
* **Function:** Utility
* **Suite:** Utility
* **Category:** Utility
* **Gear Permission:** Read-only
* **Container Level:**
  * [X] Project
  * [X] Subject
  * [X] Session
  * [X] Acquisition
  * [X] Analysis

## Inputs

### Files

#### input-file

* **Type**: `file`
* **Optional**: `false`
* **Description**: Input File

### Configuration

#### debug

* **Type**: `bool`
* **default**: `false`
* **description**: When enabled, adds debug statements in log.

#### derived metadata

* **Type**: `bool`
* **default**: `true`
* **description**: When enabled, adds derived metadata under filer.info.header.derived
  (e.g. ScanCoverage for DICOM CT)

#### siemens CSA

* **Type**: `boolean`
* **default**: `false`
* **description**: When enabled, extracts Siemens CSA header for DICOM.

#### p15E private tags

* **Type**: `boolean`
* **default**: `false`
* **description**: When enabled, extracts safe private tags for DICOM according to P15E
(<https://dicom.nema.org/medical/dicom/current/output/html/part15.html#sect_E.3.10>)

#### tag

* **Type**: `string`
* **default**: "file-metadata-importer"
* **description**: The tag to be added to the input file upon run completion.

## Outputs

The gear stores file header metadata under `file.info.header` and if `derived metadata`
is enabled, additional metadata will be stored under `file.info.header.derived`. Header
namespace is based on the file type. For example, DICOM header metadata will be stored
 under `file.info.header.dicom` and displayed under Custom Information as shown below:

![alt text](assets/images/custom-info.png)

## file.info.header structure

### DICOM

For DICOM, the following header namespace will be populated:

* All the extracted DICOM tags will be stored under `file.info.header.DICOM`.
* If `input-file` is an archive (e.g dicom.zip) or a multiframe DICOM, then
  `file.info.header.dicom_array` will be populated.
* If `Siemens CSA` is enabled, then `file.info.header.csa` will be populated if found.

#### Default DICOM Tags

By default, the following tags are not extracted:

* All private tags
* `PixelData`
* `ContourData`
* `EncryptedAttributesSequence`
* `OriginalAttributesSequence`
* `SpectroscopyData`

By default, for ZIP archive or multiframe the following tags are extracted and stored in
`file.info.header.DICOM_array`:

* `AcquisitionNumber`
* `AcquisitionTime`
* `EchoTime`
* `ImageOrientationPatient`
* `ImagePositionPatient`
* `ImageType`
* `InstanceNumber`
* `SliceLocation`
* `PhaseCount`
* `PhaseIndexes`

#### Additional DICOM Tags

Additional tags can be extracted and indexed by amending the configuration of the gear
as follow. To add or remove to that list, specific key/value must be set on the project
custom information (`project.info`) like below:

```json
context: 
   header:
      dicom:
         <DICOM-tag>: True
```

`<DICOM-tag>: True` should take the form of `<Tag>: <bool>` where `Tag` can either be a
DICOM keyword (e.g. `PatientID`), a DICOM hex index (e.g `00100020`) or a DICOM private
tag (e.g. `GEMS_PARM_01, 0043xx01` with `GEMS_PARM_01` being the private creator value
for that private tag group). See

[here](https://dicom.nema.org/dicom/2013/output/chtml/part05/sect_7.8.html) for further
explanation about private tags. If `bool` is `True`,  the tag will be added to the
parsed tags in the `file.info.header.dicom` *and* the `file.info.header.dicom_array`
namespaces; if `False` it will be removed.

#### DICOM Derived Metadata

When `derived metadata` is enabled, the following additional metadata will be stored
under `file.info.header.derived`:

* `SpacingBetweenSlices` ( Also a type 3 tag at (0018,0088))
* `MinSliceLocation` : Minimum value of slice location
* `MaxSliceLocation` : Maximum value of slice location
* `ScanCoverage` : Span of slice location (max-min)
* `SliceAxis` : 3D unit vector connecting slice origins
* `PhaseCount` : Number of sequences of consecutive slices with uniform spacing &
  orientation
* `PhaseIndexes` : If PhaseCount > 1, this array maps the slices indexes to the phase
  index
* `fov` : "Field of view" which is the 3D span in patient coordinates along x,y,z (empty
  if slice spacing is not uniform)
* `affine` : 4x4 affine transform to convert grid to patient coordinates (empty if slice
  spacing is not uniform)

#### Subject and Session Metadata

Additionally, the gear will extract subject and session information from the DICOM
tags and populate the corresponding Flywheel container attributes if not already
present.

#### Multiframe DICOM with PerFrameFunctionalGroupsSequence

In very large multiframe DICOM images, `PerFrameFunctionalGroupsSequence` can quickly
exceed available metadata space if attempted to be saved in its entirety. As of
version 1.5.2, the gear only saves the first frame of
`PerFrameFunctionalGroupsSequence`.

### NIfTI

For NIfTI, metadata will be stored under `file.info.header.nifti`.

### Bruker ParaVision

For ParaVision file, metadata will be stored under `file.info.header.paravision`.

### Siemens PTD

The raw PTD preamble will be stored under `file.info.header.ptd`.

### Philips PAR/REC

For Philips PAR/REC file, metadata will be stored under `file.info.header.par`.

### GE P-File

For GE P-File, metadata will be stored under `file.info.header.pfile`.

### EEG

For EEG, metadata will be stored under a namespace corresponding to their file type
extension, e.g `.edf` under `file.info.header.edf`.
