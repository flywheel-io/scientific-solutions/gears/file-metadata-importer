import os

from fw_gear_file_metadata_importer.files.ge import pfile_extract_files, process
from tests.conftest import ASSETS_ROOT


def test_pfile_extract_files(tmpdir):
    file_name = ASSETS_ROOT / "PFILE/21580_20_1_pfile.7.zip"
    assert os.path.isfile(file_name)

    pfile_path = pfile_extract_files(file_name, tmpdir)

    assert isinstance(pfile_path, str)


def test_process():
    file_name = ASSETS_ROOT / "PFILE/21580_20_1_pfile.7.zip"

    fe, meta, qc = process(file_name)

    assert fe["info"]["header"]["pfile"]["PulseSequenceName"] == "nfl_press_v1"
    assert fe["info"]["header"]["pfile"]["SeriesDescription"] == "acc nfl"
