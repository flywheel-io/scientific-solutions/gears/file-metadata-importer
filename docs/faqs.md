# Frequently Asked Questions

??? info "How can I extract additional DICOM tags such as private tags?"
    The file-metadata-importer gear does not extract private tags by default. However,
    you can enable this by specifying which private tags you want to extract be
    configuring it at the project level as described
    [here](details.md#additional-dicom-tags).

??? info "How can I add tags in the `dicom_array` namespace?"
    The file-metadata-importer gear extracts a subset of DICOM tags that varies
    across the series and stores them in the `dicom_array` namespace. You can add or
    remove tags from this list by configuring it at the project level as described
    [here](details.md#additional-dicom-tags).

??? info "Can I extract Siemens CSA header of DICOM?"
    The file-metadata-importer gear does not extract Siemens CSA header by default.
    However, you can enable this by enabling the `siemens-csa` configuration option.

??? info "What resources are needed to run the gear?"
    This gear can be run on any instance type and does not require any specialized
    hardware.

??? info "How long does it take to run the gear?"
    This gear usually takes a few seconds (<10s) to run.
